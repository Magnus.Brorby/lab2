package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {

    int maks = 20;
    ArrayList<FridgeItem> itemList = new ArrayList<FridgeItem>();

    @Override
    public int totalSize() {
        return maks;
    }

    @Override
    public int nItemsInFridge() {
        int amount = itemList.size();
        return amount;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            itemList.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemList.contains(item)) {
            itemList.remove(item);
            
        }
        else {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        itemList.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem fridgeItem : itemList) {
            if (fridgeItem.hasExpired()) {
                expiredFood.add(fridgeItem);
            }
        }
        for (FridgeItem expiredItem : expiredFood) {
            itemList.remove(expiredItem);
        }
        return expiredFood;
    }

}
